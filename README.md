This is the reference implementation for https://gitlab.com/gitlab-org/container-registry/-/issues/196.

To test this, make sure you have Go >=1.14 and run:

```sh
export GOOGLE_APPLICATION_CREDENTIALS="/path/to/gcs/service/account/credentials.json"
export GCS_BUCKET="gcs-bucket-name"
git clone https://gitlab.com/jdrpereira/blob-enum-gcs
cd blob-enum-gcs
go build -o enum .
./enum
```

module gitlab.com/jdrpereira/blob-enum-gcs

go 1.14

require (
	cloud.google.com/go/storage v1.10.0
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	google.golang.org/api v0.30.0
)

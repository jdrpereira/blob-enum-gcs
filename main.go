package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"google.golang.org/api/iterator"
)

type blob struct {
	digest      string
	size        int64
	contentType string
	createdAt   time.Time
}

func scanPartition(ctx context.Context, bucket *storage.BucketHandle, partition string, ch chan blob) error {
	q := &storage.Query{
		Prefix: fmt.Sprintf("docker/registry/v2/blobs/sha256/%s/", partition),
	}
	if err := q.SetAttrSelection([]string{"Name", "ContentType", "Size", "Created"}); err != nil {
		return err
	}

	it := bucket.Objects(ctx, q)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.WithField("partition", q.Prefix).WithError(err).Error("failed to list partition")
			return err
		}

		if strings.HasSuffix(attrs.Name, "/data") {
			digest := strings.TrimPrefix(strings.TrimSuffix(attrs.Name, "/data"), q.Prefix)

			ch <- blob{
				digest:      fmt.Sprintf("sha256:%s", digest),
				size:        attrs.Size,
				contentType: attrs.ContentType,
				createdAt:   attrs.Created,
			}
		}
	}

	return nil
}

func enumerate(ctx context.Context, bucket string, ch chan blob) error {
	client, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}
	defer client.Close()

	alpha := "abcdefghijklmnopqrstuvwxyz0123456789"
	partitions := make([]string, 0, len(alpha)*len(alpha))

	for _, char1 := range alpha {
		for _, char2 := range alpha {
			partitions = append(partitions, fmt.Sprintf("%c%c", char1, char2))
		}
	}
	sort.Strings(partitions)

	g, gctx := errgroup.WithContext(ctx)

	for i, p := range partitions {
		log.WithFields(log.Fields{"count": i, "partition": p}).Debug("spawning partition scanning goroutine")
		func(p string) {
			g.Go(func() error {
				return scanPartition(gctx, client.Bucket(bucket), p, ch)
			})
		}(p)
	}

	return g.Wait()
}

func main() {
	bucket := os.Getenv("GCS_BUCKET")
	if bucket == "" {
		log.Error("GCS_BUCKET environment variable not set")
		os.Exit(1)
	}

	if os.Getenv("GOOGLE_APPLICATION_CREDENTIALS") == "" {
		log.Error("GOOGLE_APPLICATION_CREDENTIALS environment variable not set")
		os.Exit(1)
	}

	val := os.Getenv("LOG_LEVEL")
	if val == "" {
		val = log.InfoLevel.String()
	}
	lvl, err := log.ParseLevel(val)
	if err != nil {
		log.WithField("value", val).WithError(err).Error("LOG_LEVEL environment variable value is not a valid log level")
		os.Exit(1)
	}
	log.SetLevel(lvl)

	var count int
	var totalSize int64

	blobCh := make(chan blob)
	done := make(chan struct{})

	go func() {
		for blob := range blobCh {
			count++
			totalSize += blob.size

			if log.IsLevelEnabled(log.DebugLevel) {
				go log.WithFields(log.Fields{
					"digest":       blob.digest,
					"content_type": blob.contentType,
					"size":         blob.size,
					"created_at":   blob.createdAt,
				}).Debug("blob")
			}
		}
		done <- struct{}{}
	}()

	ctx := context.Background()
	start := time.Now()

	log.Info("starting")

	if err := enumerate(ctx, bucket, blobCh); err != nil {
		if errors.Is(err, context.Canceled) {
			log.WithError(err).Error("context was canceled")
		} else {
			log.WithError(err).Error("error enumerating blobs")
		}
	}
	close(blobCh)
	<-done

	log.WithFields(log.Fields{
		"duration_s":            time.Since(start),
		"blob_count":            count,
		"blob_total_size_bytes": totalSize,
	}).Info("complete")
}
